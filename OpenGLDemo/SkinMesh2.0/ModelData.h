#pragma once
#include <algorithm>
#include <vector>
#include <map>
#include <memory>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <fbxsdk.h>


class Bone
{
public:
	typedef struct
	{
		glm::mat4 worldMat;
		glm::mat4 localMat;
		glm::vec3 localTranslation;
		glm::quat localRotate;
		glm::vec3 localSacle;
	} FrameData;

	Bone()
	{
		boneID = 0;
		parentID = 0;
		indexInKeyFrames = 0;
		geometricMat = glm::mat4(1.0);
		vertexsToBoneMat = glm::mat4(1.0);
	};

	~Bone()
	{
		childrenID.clear();
	}

	static void CalLocalMat(FrameData& fdata)
	{
		fdata.localMat = glm::translate(glm::mat4(1.0), fdata.localTranslation) * glm::mat4_cast(fdata.localRotate) * glm::scale(glm::mat4(1.0), fdata.localSacle);
	}

	unsigned int boneID;						//唯一Id
	unsigned int parentID;						//父节点
	std::vector<unsigned int> childrenID;		//子节点
												
	FrameData currentPosture;					//当前姿态
	unsigned int indexInKeyFrames;
	glm::mat4 geometricMat;						//偏移(兼容3dmax等软件)
	glm::mat4 vertexsToBoneMat;					//用于顶点蒙皮坐标转换
};

class Skeleton
{
	typedef std::map<unsigned int, Bone> BoneMap;
public:
	Skeleton() {};
	~Skeleton()
	{
		_bones.clear();
		_rootMats.clear();
	}

	void CalRootMats()
	{
		_rootMats.clear();
		for (auto& iter = _bones.cbegin(); iter != _bones.cend(); ++iter)
		{
			if (0 == iter->second.parentID && !iter->second.childrenID.empty())
			{
				glm::mat4 mat = iter->second.currentPosture.worldMat * glm::inverse(iter->second.currentPosture.localMat);
				_rootMats.insert(std::make_pair(iter->first, mat));
			}
		}
	}

	BoneMap _bones;
	std::map<unsigned int, glm::mat4> _rootMats;
};


template<typename T>
class KeyFrame
{
public:
	KeyFrame(float t,T val) 
	{
		m_time = t;
		m_value = val;
	}
	~KeyFrame() {}
public:
	T		m_value;
	float	m_time;
};

template<typename T>
class KeyFrames
{
	typedef T (*InterpolateFunc)(float, const KeyFrame<T>&, const KeyFrame<T>&);
public:
	KeyFrames()
	{
		startTime = 0.0;
		endTime = 0.0;
		GreateInterpolateFun(); 
	}

	virtual ~KeyFrames()
	{
		m_keyFrames.clear();
	}
	void setInterpolateFunc(InterpolateFunc func) { interpolateFunc = func; }
	void addKeyFrame(const KeyFrame<T>& key)
	{
		m_keyFrames.push_back(key);
	}
	bool removeKeyFrame(unsigned int index)
	{
		if (index >= m_keyFrames.size())
			return false;
		m_keyFrames.erase(m_keyFrames.begin() + index);
		return true;
	}
	std::vector<KeyFrame<T>>& getKeyFrames() { return m_keyFrames; }
	const std::vector<KeyFrame<T>>& getKeyFrames() const{ return m_keyFrames; }
	void sortKeys()
	{
		sort(m_keyFrames.begin(), m_keyFrames.end(), [](const KeyFrame<T>& val1, const KeyFrame<T>& val2) {
			return val1.m_time < val2.m_time;
		});
	}

	void CalValue(float time,T& value)
	{
		if (time < m_keyFrames.front().m_time)
		{
			value = m_keyFrames.front().m_value;
			return;
		}
		if (time > m_keyFrames.back().m_time)
		{
			value = m_keyFrames.back().m_value;
			return;
		}

		unsigned int f = 0;
		unsigned int b = m_keyFrames.size() - 1;
		unsigned int mid = (f + b) / 2;
		while (f != mid)
		{
			if (time > m_keyFrames[mid].m_time)
			{
				f = mid;
			}
			else
			{
				b = mid;
			}
			mid = (f + b) / 2;
		}
		const KeyFrame<T>& key1 = m_keyFrames[f];
		const KeyFrame<T>& key2 = m_keyFrames[f + 1];
		//根据key1,key2插值
		if (interpolateFunc)
			value = interpolateFunc(time, key1, key2);
	}

	float getStartTime() { return startTime; }
	float getEndTime() { return endTime; }

	void CalStartTimeAndEndTime()
	{
		sortKeys();
		startTime = m_keyFrames.front().m_time;
		endTime = m_keyFrames.back().m_time;
	}

protected:
	void GreateInterpolateFun()
	{
		interpolateFunc =
			[](float time, const KeyFrame<T>& key1, const KeyFrame<T>& key2)
		{
			//float blend = (time - key1.m_time) / (key2.m_time - key1.m_time);
			//T val = key1.m_value*(1 - blend) + key2.m_value * blend;
			return key1.m_value;
		};
	}

	float startTime;
	float endTime;
	InterpolateFunc interpolateFunc;
	std::vector<KeyFrame<T>> m_keyFrames;
};

template <typename T> 
class Animation
{
public:
	typedef typename KeyFrames<T> Channel;
	typedef std::vector<Channel> Channels;
public:

	enum PlayMode
	{
		ONCE,
		LOOP,
	};

	Animation()
	{
		_startTime = 0.0; 
		_duration = 0.0;
		_playmode = ONCE;
		_pause = false;
		_resetPauseTime = true;
		_pauseTime = 0.0;
		_pauseDuration = 0.0;
	}

	virtual ~Animation() { _AnimStacks.clear(); }

	std::vector<Channels>& getAnimStacks() { return _AnimStacks; };
	const std::vector<Channels>& getAnimStacks() const { return _AnimStacks; };
	Channels& getCurrentChannels()
	{
		if (_AnimStacks.empty())
			return Channels();
		
		return _AnimStacks[_currentAnimation];
	}
	const Channels& getCurrentChannels() const
	{
		if (_AnimStacks.empty())
			return Channels();
		
		return _AnimStacks[_currentAnimation];
	}

	void setCurrentAnimation(unsigned int index)
	{
		if (index >= getAnimStacksCount()) return;

		_currentAnimation = index; 
		_pause = false;
		_resetPauseTime = true;
		_pauseTime = 0.0;
		_pauseDuration = 0.0;
		computeStartTimeAndDurationFromCurrentChannels();
	}
	unsigned int getCurrentAnimation() { return _currentAnimation; }

	unsigned int getAnimStacksCount() const { return _AnimStacks.size(); }
	unsigned int getCurrentChannelsCount() const { return _AnimStacks[_currentAnimation].size(); }

	void addAnimStack() { _AnimStacks.push_back(Channels()); }
	void addAnimStack(const Channels& channels) { _AnimStacks.push_back(channels); }

	void addChannel(const Channel& channel) { _AnimStacks[_currentAnimation].push_back(channel); }
	bool removeChannel(unsigned int index)
	{ 
		if (index >= _AnimStacks[_currentAnimation].size())
			return false;
		
		_AnimStacks[_currentAnimation].erase(_AnimStacks[_currentAnimation].begin() + index);
		return true;
	}

	void setPlayMode(PlayMode mode) { _playmode = mode; }
	PlayMode getPlayMode() const { return _playmode; }

	void computeStartTimeAndDurationFromCurrentChannels()
	{
		if (_AnimStacks[_currentAnimation].empty())
			return;

		double tmin = 1e5;
		double tmax = -1e5;

		for (auto& chan = _AnimStacks[_currentAnimation].begin(); chan != _AnimStacks[_currentAnimation].end(); chan++)
		{
			chan->CalStartTimeAndEndTime();
			float min = chan->getStartTime();
			if (min < tmin)
				tmin = min;
			float max = chan->getEndTime();
			if (max > tmax)
				tmax = max;
		}

		_startTime = tmin;
		_duration= tmax - tmin;
	}

	float getStartTime() { return _startTime; };
	float getDuration() { return _duration; };
	bool isPause() { return _pause; };
	void Pause(bool pause) { _pause = pause; };
	virtual void update(float time) = 0;

protected:
	float _startTime;
	float _duration;
	bool _pause;
	bool _resetPauseTime;
	float _pauseTime;
	float _pauseDuration;
	PlayMode _playmode;

	std::vector<Channels> _AnimStacks;
	unsigned int _currentAnimation;
};

class SkeletonAnimation :public Animation<Bone::FrameData>
{
public:
	SkeletonAnimation() {};
	virtual ~SkeletonAnimation()
	{
		_skeletonWorldMat.clear();
	}

	virtual void update(float time)
	{
		if (_pause)
		{
			if (_resetPauseTime)
			{
				_resetPauseTime = false;
				_pauseTime = time - _pauseDuration;
			}
			_pauseDuration = time - _pauseTime;
		}
		else
		{
			time = time - _pauseDuration;
			_resetPauseTime = true;
		}

		if (_pause)	time = _pauseTime;

		if (time < _startTime) time = _startTime;

		float tt = 0.0;
		switch (_playmode)
		{
		case LOOP:
		{
			int count = int(time / _duration);
			tt = time - _duration * count;
			break;
		}
		default:
		{
			tt = time;
			break;
		}
		}

		updateSkeleton(tt);
	}

	void addBone(const Bone& bone)
	{
		_skeleton._bones.insert(std::make_pair(bone.boneID, bone));
	}

	Skeleton& getSkeleton() { return _skeleton; }
	const Skeleton& getSkeleton() const{ return _skeleton; }

	const std::vector<glm::mat4>& getSkeletonPostures()
	{
		_skeletonWorldMat.clear();
		_skeletonWorldMat.reserve(_skeleton._bones.size());
		for (auto& iter = _skeleton._bones.cbegin(); iter != _skeleton._bones.cend(); ++iter)
		{
			_skeletonWorldMat.push_back(iter->second.currentPosture.worldMat * iter->second.geometricMat * iter->second.vertexsToBoneMat);
		}

		return _skeletonWorldMat;
	}

	const std::map<unsigned int, glm::mat4>& getRootBonePostures() { return _skeleton._rootMats; };

protected:
	void updateSkeleton(float time)
	{
		for (auto& iter = _skeleton._rootMats.cbegin(); iter != _skeleton._rootMats.cend(); ++iter)
		{
			updateBone(time, _skeleton._bones[iter->first]);
		}
	}

	void updateBone(float time, Bone& bone)
	{
		_AnimStacks[_currentAnimation][bone.indexInKeyFrames].CalValue(time, bone.currentPosture);
		if (0 == bone.parentID)
		{
			bone.currentPosture.worldMat = /*glm::rotate(glm::mat4(1.0), glm::radians(180.0f), glm::vec3(0.0f, 1.0f, 0.0f)) **/ _skeleton._rootMats[bone.boneID] * bone.currentPosture.localMat;
		}
		else
		{
			bone.currentPosture.worldMat = _skeleton._bones[bone.parentID].currentPosture.worldMat * bone.currentPosture.localMat;
		}

		if (!bone.childrenID.empty())
		{
			for (unsigned int i = 0; i < bone.childrenID.size(); ++i)
			{
				updateBone(time, _skeleton._bones[bone.childrenID[i]]);
			}
		}
	}

	std::vector<glm::mat4> _skeletonWorldMat;
	Skeleton _skeleton;
};

const int NUM_BONES_PER_VEREX = 12;
struct Vertex
{
	Vertex() :pos(glm::vec3(0.0)), texcoord(glm::vec2(0.0))
		, bonesID(glm::vec4(-1.0)), bonesIndexInFrames(glm::ivec4(0)), bonesWeight(glm::vec4(0.0))
		, bonesID2(glm::vec4(-1.0)), bonesIndexInFrames2(glm::ivec4(0)), bonesWeight2(glm::vec4(0.0))
		, bonesID3(glm::vec4(-1.0)), bonesIndexInFrames3(glm::ivec4(0)), bonesWeight3(glm::vec4(0.0)) {};
	glm::vec3 pos; // 顶点在模型空间中的位置
	glm::vec3 normal; // 顶点对应法线
	glm::vec2 texcoord;// 纹理坐标
	glm::vec4 bonesID;
	glm::ivec4 bonesIndexInFrames;
	glm::vec4 bonesWeight;

	glm::vec4 bonesID2;
	glm::ivec4 bonesIndexInFrames2;
	glm::vec4 bonesWeight2;

	glm::vec4 bonesID3;
	glm::ivec4 bonesIndexInFrames3;
	glm::vec4 bonesWeight3;

	int index;
};

struct Mesh
{
	std::vector<Vertex> m_Vertex;//点
	std::string shader; // 纹理文件名
	unsigned int texID; //纹理缓冲对象
	glm::mat4 m_worldMat;//世界坐标矩阵
	glm::mat4 m_linkMat;//转换到link空间（我的理解是：fbx中将bone和mesh放在link空间中蒙皮，link空间相当于一个中间桥梁）
	unsigned int VAO, VBO/*, EBO*/;// 顶点数组对象，顶点缓冲对象，索引缓冲对象
};


struct StateSetContent
{
	StateSetContent()
		: diffuseFactor(1.0),
		reflectionFactor(1.0),
		emissiveFactor(1.0),
		ambientFactor(1.0)
	{
	}

	// more textures types here...
	std::string diffuseTextureName;
	std::string opacityTextureName;
	std::string reflectionTextureName;
	std::string emissiveTextureName;
	std::string ambientTextureName;
	// textures maps channels names...
	std::string diffuseChannel;
	std::string opacityChannel;
	std::string reflectionChannel;
	std::string emissiveChannel;
	std::string ambientChannel;
	// more channels names here...

	// combining factors...
	double diffuseFactor;
	double reflectionFactor;
	double emissiveFactor;
	double ambientFactor;
	// more combining factors here...

	double diffuseScaleU;
	double diffuseScaleV;
	double opacityScaleU;
	double opacityScaleV;
	double emissiveScaleU;
	double emissiveScaleV;
	double ambientScaleU;
	double ambientScaleV;

	enum TextureUnit
	{
		DIFFUSE_TEXTURE_UNIT = 0,
		OPACITY_TEXTURE_UNIT,
		REFLECTION_TEXTURE_UNIT,
		EMISSIVE_TEXTURE_UNIT,
		AMBIENT_TEXTURE_UNIT
		// more texture units here...
	};
};
typedef std::map<const FbxSurfaceMaterial *, StateSetContent> FbxMaterialMap;
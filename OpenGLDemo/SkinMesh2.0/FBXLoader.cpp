#include <iostream>
#include<windows.h>
#include <fstream>  
#include <string>  

#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <stb_image.h>
#include "ModelData.h"
#include "FBXLoader.h"

StateSetContent convert(const FbxSurfaceMaterial* pFbxMat);
std::string getUVChannelForTextureMap(std::vector<StateSetContent>& stateSetList, const char* pName);
const FbxLayerElementUV* getUVElementForChannel(std::string uvChannelName, FbxLayerElement::EType elementType, FbxMesh* pFbxMesh);
template <typename T>
bool layerElementValid(const FbxLayerElementTemplate<T>* pLayerElement);
template <typename FbxT>
FbxT getElement(const FbxLayerElementTemplate<FbxT>* pLayerElement, const FbxMesh* fbxMesh, int nPolygon, int nPolyVertex, int nMeshVertex);
template <typename T>
int getVertexIndex(const FbxLayerElementTemplate<T>* pLayerElement, const FbxMesh* fbxMesh, int nPolygon, int nPolyVertex, int nMeshVertex);
std::string  _u8ToAnsi(const std::string & str);
std::string string_To_UTF8(const std::string & str);
FbxMaterialMap       _fbxMaterialMap;

void FbxMatrixToGLMMatrix(const FbxAMatrix& fbxMatrix, glm::mat4& mat)
{
	for (int i = 0; i < 4; ++i)
	{
		for (int j = 0; j < 4; ++j)
		{
			mat[i][j] = fbxMatrix[i][j];
		}
	}
}


Bone::FrameData CalBoneFrameData(float time, const KeyFrame<Bone::FrameData>& key1, const KeyFrame<Bone::FrameData>& key2)
{
	Bone::FrameData boneFrameData;

	float blend = (time - key1.m_time) / (key2.m_time - key1.m_time);
	boneFrameData.localTranslation = key1.m_value.localTranslation * (1 - blend) + key2.m_value.localTranslation * blend;
	boneFrameData.localSacle = key1.m_value.localSacle * (1 - blend) + key2.m_value.localSacle * blend;
	boneFrameData.localRotate = glm::slerp(key1.m_value.localRotate, key2.m_value.localRotate, blend);
	Bone::CalLocalMat(boneFrameData);

	return boneFrameData;
}


FBXLoader::FBXLoader()
{
	m_fbxMgr = 0;
	m_scene = 0;
}

FBXLoader::~FBXLoader()
{
	if (m_scene)
	{
		m_scene->Destroy();
		m_scene = 0;
	}
	if (m_fbxMgr)
	{
		m_fbxMgr->Destroy();
		m_fbxMgr = 0;
	}

	m_MeshVec.clear();
	m_BonesInverse.clear();
}

bool FBXLoader::Init()
{
	m_fbxMgr = FbxManager::Create();
	if (!m_fbxMgr)
	{
		FBXSDK_printf("Error: Unable to create FBX manager!\n");
		return false;
	}

	FbxIOSettings* ios = FbxIOSettings::Create(m_fbxMgr, IOSROOT);
	m_fbxMgr->SetIOSettings(ios);

	m_scene = FbxScene::Create(m_fbxMgr, "My Scene");
	if (!m_scene)
	{
		FBXSDK_printf("Error: Unable to create FBX scene!\n");
		return false;
	}

	return true;
}

const std::vector<Mesh>& FBXLoader::getMeshs()
{
	return m_MeshVec;
}

bool FBXLoader::LoadModel(const char* pFilename)
{
	FbxString retStr = "";
	char * newStr = NULL;
	FbxAnsiToUTF8(pFilename, newStr);// Fbx Sdk 提供的字符编码转换API
	if (NULL != newStr)
	{
		retStr = newStr;
		delete[] newStr;
		newStr = NULL;
	}
	if (retStr == "")
	{
		return false;
	}

	FbxImporter* lImporter = FbxImporter::Create(m_fbxMgr, "");
	if (!lImporter->Initialize(retStr, -1, m_fbxMgr->GetIOSettings()))
	{
		return false;
	}
	if (!lImporter->IsFBX())
	{
		return false;
	}
	for (int i = 0; FbxTakeInfo* lTakeInfo = lImporter->GetTakeInfo(i); i++)
	{
		lTakeInfo->mSelect = true;
	}
	if (!lImporter->Import(m_scene))
		return false;

	FbxNode* pNode = m_scene->GetRootNode();
	if (!pNode) return false;

	m_numStacks = m_scene->GetSrcObjectCount<FbxAnimStack>();
	for (int i = 0; i < m_numStacks; ++i)
	{
		m_Animation.addAnimStack();
	}

	//FbxAxisSystem lAxisSytemReconstruct(FbxAxisSystem::EPreDefinedAxisSystem::eMax);
	//lAxisSytemReconstruct.ConvertScene(m_scene);

	int nChildCount = pNode->GetChildCount();
	for (int i = 0; i < nChildCount; ++i)
	{
		FbxNode* pChildNode = pNode->GetChild(i);
		ReadNode(pChildNode);
	}

	if (m_Animation.getSkeleton()._bones.empty())
		return true;

	for (auto& iter= m_Animation.getSkeleton()._bones.begin(); iter!= m_Animation.getSkeleton()._bones.end(); ++iter)
	{
		if (m_BonesInverse.end() == m_BonesInverse.find(iter->first))continue;
		iter->second.vertexsToBoneMat = m_BonesInverse.find(iter->first)->second;
	}

	for (auto& iter = m_Animation.getSkeleton()._bones.crbegin(); iter != m_Animation.getSkeleton()._bones.crend(); ++iter)
	{
		if (0 == iter->second.parentID) continue;
		m_Animation.getSkeleton()._bones[iter->second.parentID].childrenID.push_back(iter->first);
	}

	m_Animation.getSkeleton().CalRootMats();

	for (int i = 0; i < m_MeshVec.size(); ++i)
	{
		std::vector<Vertex>& vertexs = m_MeshVec[i].m_Vertex;
		for (auto&x : vertexs)
		{
			for (unsigned int j = 0; j < NUM_BONES_PER_VEREX; ++j)
			{
				if (j < 4)
				{
					if (x.bonesID[j] < 0) break;
					unsigned int k = 0;
					for (auto& iter = m_Animation.getSkeleton()._bones.cbegin(); iter != m_Animation.getSkeleton()._bones.cend(); ++iter, ++k)
					{
						if (x.bonesID[j] == iter->first)
						{
							x.bonesIndexInFrames[j] = k;
							break;
						}
					}
				}
				else if (j < 8)
				{
					if (x.bonesID2[j - 4] < 0) break;
					unsigned int k = 0;
					for (auto& iter = m_Animation.getSkeleton()._bones.cbegin(); iter != m_Animation.getSkeleton()._bones.cend(); ++iter, ++k)
					{
						if (x.bonesID2[j - 4] == iter->first)
						{
							x.bonesIndexInFrames2[j - 4] = k;
							break;
						}
					}
				}
				else
				{
					if (x.bonesID3[j - 8] < 0) break;
					unsigned int k = 0;
					for (auto& iter = m_Animation.getSkeleton()._bones.cbegin(); iter != m_Animation.getSkeleton()._bones.cend(); ++iter, ++k)
					{
						if (x.bonesID3[j - 8] == iter->first)
						{
							x.bonesIndexInFrames3[j - 8] = k;
							break;
						}
					}
				}
			}
		}
	}

	return true;
}

void FBXLoader::ReadNode(FbxNode* pNode)
{
	int nChildCount = pNode->GetChildCount();
	for (int i = 0; i < nChildCount; ++i)
	{
		FbxNode* pChildNode = pNode->GetChild(i);
		ReadNode(pChildNode);
	}

	if (FbxNodeAttribute* lNodeAttribute = pNode->GetNodeAttribute())
	{
		FbxNodeAttribute::EType attrType = lNodeAttribute->GetAttributeType();
		switch (attrType)
		{
		case FbxNodeAttribute::eSkeleton:
		{
			FbxSkeleton* pSkeleton = FbxCast<FbxSkeleton>(pNode->GetNodeAttribute());
			if (pSkeleton == NULL)	return;
			
			unsigned int skeletonId = pNode->GetUniqueID();
			Bone bframe;
			bframe.boneID = skeletonId;

			if (FbxCast<FbxSkeleton>(pNode->GetParent()->GetNodeAttribute()))
			{
				bframe.parentID = pNode->GetParent()->GetUniqueID();
			}

			auto matT = pNode->GeometricTranslation.Get();
			auto matR = pNode->GeometricRotation.Get();
			auto matS = pNode->GeometricScaling.Get();
			FbxAMatrix geometricMat(matT, matR, matS);
			FbxMatrixToGLMMatrix(geometricMat, bframe.geometricMat);

			for (int iStack = 0; iStack < m_numStacks; ++iStack)
			{
				FbxTimeSpan timeInterval;
				FbxAnimStack* pAnimStack = FbxCast<FbxAnimStack>(m_scene->GetSrcObject<FbxAnimStack>(iStack));
				m_scene->GetRootNode()->GetAnimationInterval(timeInterval, pAnimStack);
				m_scene->SetCurrentAnimationStack(pAnimStack);

				auto start = timeInterval.GetStart();
				auto end = timeInterval.GetStop();
				auto duration = timeInterval.GetDuration();
				auto frameCount = duration.GetFrameCount();
				auto oneFrameValue = duration.GetOneFrameValue();

				m_Animation.setCurrentAnimation(iStack);
				KeyFrames<Bone::FrameData> kfs;
				kfs.setInterpolateFunc(CalBoneFrameData);

				FbxTime keyTimer;
				FbxLongLong millseconds;

				for (UINT i = 0; i < frameCount; ++i)
				{
					millseconds = start.Get() + (float)i * oneFrameValue;
					keyTimer.Set(millseconds);
					auto curveKeyGlobalMatrix = pNode->EvaluateGlobalTransform(keyTimer);
					auto curveKeyLocalMatrix = pNode->EvaluateLocalTransform(keyTimer);
					auto localTrans = pNode->EvaluateLocalTranslation(keyTimer);
					auto localScale = pNode->EvaluateLocalScaling(keyTimer);
					auto localquat = curveKeyLocalMatrix.GetQ();

					Bone::FrameData framedata;
					FbxMatrixToGLMMatrix(curveKeyGlobalMatrix, framedata.worldMat);
					FbxMatrixToGLMMatrix(curveKeyLocalMatrix, framedata.localMat);

					framedata.localTranslation = glm::vec3(localTrans[0], localTrans[1], localTrans[2]);
					framedata.localRotate = glm::quat(localquat[3], localquat[0], localquat[1], localquat[2]);
					framedata.localSacle = glm::vec3(localScale[0], localScale[1], localScale[2]);

					keyTimer.Set(millseconds - start.Get());
					kfs.addKeyFrame(KeyFrame<Bone::FrameData>(keyTimer.GetSecondDouble(), framedata));
				}
				m_Animation.addChannel(kfs);
			}

			if (m_Animation.getAnimStacksCount() > 0)
			{
				bframe.indexInKeyFrames = m_Animation.getCurrentChannelsCount() - 1;
				bframe.currentPosture = m_Animation.getCurrentChannels()[bframe.indexInKeyFrames].getKeyFrames().front().m_value;
			}
			m_Animation.addBone(bframe);
		}
		break;
		case FbxNodeAttribute::eMesh:
		{
			Mesh intimesh;
			FbxMesh* pMesh = FbxCast<FbxMesh>(pNode->GetNodeAttribute());
			if (pMesh == NULL)	return;
			std::string str = pMesh->GetName();

			m_MeshVec.push_back(intimesh);
			Mesh& mesh = m_MeshVec.back();

			//auto mat = pNode->EvaluateLocalTransform();
			auto mat = pNode->EvaluateGlobalTransform();
			auto matT = pNode->GeometricTranslation.Get();
			auto matR = pNode->GeometricRotation.Get();
			auto matS = pNode->GeometricScaling.Get();
			FbxAMatrix geometricMat(matT, matR, matS);
			mat = mat * geometricMat;
			for (int i = 0; i < 4; ++i)
			{
				for (int j = 0; j < 4; ++j)
				{
					mesh.m_worldMat[i][j] = mat[i][j];
				}
			}
			
			unsigned nMaterials = pNode->GetMaterialCount();
			std::vector<StateSetContent > stateSetList;
			for (unsigned i = 0; i < nMaterials; ++i)
			{
				FbxSurfaceMaterial* fbxMaterial = pNode->GetMaterial(i);
				stateSetList.push_back(convert(fbxMaterial));
			}

			for (unsigned int i = 0; i < stateSetList.size(); i++)
			{
				mesh.shader = stateSetList[i].diffuseTextureName;
			}


			//std::fstream file(_u8ToAnsi(mesh.shader), std::ios::in | std::ios::out | std::ios::binary);
			//if (file)
			//{
			//	char* pch;
			//	long size;
			//	//file.read()
			//	int	l = file.tellg();
			//	file.seekg(0, std::ios::end);
			//	int	m = file.tellg();
			//	size = m - l;
			//	file.seekg(0, std::ios::beg);
			//	pch = new char[size];
			//	file.read(pch, size);
			//	file.close();
			//}


			// load and create a texture 
			// -------------------------
			unsigned int texture;
			// texture
			// ---------
			glGenTextures(1, &texture);
			glBindTexture(GL_TEXTURE_2D, texture);
			// set the texture wrapping parameters
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
			// set texture filtering parameters
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
			// load image, create texture and generate mipmaps
			int width, height, nrChannels;
			stbi_set_flip_vertically_on_load(true); // tell stb_image.h to flip loaded texture's on the y-axis.
			unsigned char *data = stbi_load(_u8ToAnsi(mesh.shader).c_str(), &width, &height, &nrChannels, 0);

			int format = nrChannels == 3 ? GL_RGB : GL_RGBA;
			if (data)
			{
				glTexImage2D(GL_TEXTURE_2D, 0, format, width, height, 0, format, GL_UNSIGNED_BYTE, data);
				glGenerateMipmap(GL_TEXTURE_2D);
				mesh.texID = texture;
			}
			else
			{
				mesh.texID = 0;
				std::cout << "Failed to load texture" << std::endl;
			}
			stbi_image_free(data);

			if (!pMesh->IsTriangleMesh())
			{
				FbxGeometryConverter converter(m_fbxMgr);
				pMesh = FbxCast<FbxMesh>(converter.Triangulate(pMesh, true));
			}

			//不同方式求法线
			//int ncont = pMesh->GetElementNormalCount();
			//FbxGeometryElementNormal* leNormal = pMesh->GetElementNormal(0);
			//FbxArray<FbxVector4> pNormals;
			//pMesh->GetPolygonVertexNormals(pNormals);

			const FbxLayerElementNormal* pFbxNormals = 0;
			//const FbxLayerElementVertexColor* pFbxColors = 0;
			//const FbxLayerElementMaterial* pFbxMaterials = 0;

			for (int cLayerIndex = 0; cLayerIndex < pMesh->GetLayerCount(); cLayerIndex++)
			{
				const FbxLayer* pFbxLayer = pMesh->GetLayer(cLayerIndex);
				if (!pFbxLayer)
					continue;

				if (!pFbxNormals)
					pFbxNormals = pFbxLayer->GetNormals();
				//if (!pFbxColors)
				//	pFbxColors = pFbxLayer->GetVertexColors();
				//if (!pFbxMaterials)
				//	pFbxMaterials = pFbxLayer->GetMaterials();
			}
			if (!layerElementValid(pFbxNormals)) pFbxNormals = 0;

			std::string diffuseChannel = getUVChannelForTextureMap(stateSetList, FbxSurfaceMaterial::sDiffuse);
			const FbxLayerElementUV* pFbxUVs_diffuse = getUVElementForChannel(diffuseChannel, FbxLayerElement::eTextureDiffuse, pMesh);
			if (!layerElementValid(pFbxUVs_diffuse)) pFbxUVs_diffuse = 0;

			int deformerCount = pMesh->GetDeformerCount();
			const FbxVector4* pFbxVertices = pMesh->GetControlPoints();
			int nPolys = pMesh->GetPolygonCount();
			for (int i = 0, nVertex = 0; i < nPolys; ++i)
			{
				int lPolygonSize = pMesh->GetPolygonSize(i);
				if (lPolygonSize == 3)
				{
					// Triangle
					int v0 = pMesh->GetPolygonVertex(i, 0),
						v1 = pMesh->GetPolygonVertex(i, 1),
						v2 = pMesh->GetPolygonVertex(i, 2);

					Vertex vert1, vert2, vert3;
					FbxVector4 vertices0 = pFbxVertices[v0];
					FbxVector4 vertices1 = pFbxVertices[v1];
					FbxVector4 vertices2 = pFbxVertices[v2];
					vert1.index = v0;
					vert2.index = v1;
					vert3.index = v2;
					vert1.pos = glm::vec3(vertices0[0], vertices0[1], vertices0[2]);
					vert2.pos = glm::vec3(vertices1[0], vertices1[1], vertices1[2]);
					vert3.pos = glm::vec3(vertices2[0], vertices2[1], vertices2[2]);

					if (pFbxNormals)
					{
						FbxVector4 normal1 = getElement(pFbxNormals, pMesh, i, 0, nVertex);
						FbxVector4 normal2 = getElement(pFbxNormals, pMesh, i, 1, nVertex + 1);
						FbxVector4 normal3 = getElement(pFbxNormals, pMesh, i, 2, nVertex + 2);

						vert1.normal = glm::vec3(normal1[0], normal1[1], normal1[2]);
						vert2.normal = glm::vec3(normal2[0], normal2[1], normal2[2]);
						vert3.normal = glm::vec3(normal3[0], normal3[1], normal3[2]);
					}

					if (pFbxUVs_diffuse != NULL)
					{
						FbxVector2 V1 = getElement(pFbxUVs_diffuse, pMesh, i, 0, nVertex);
						FbxVector2 V2 = getElement(pFbxUVs_diffuse, pMesh, i, 1, nVertex + 1);
						FbxVector2 V3 = getElement(pFbxUVs_diffuse, pMesh, i, 2, nVertex + 2);
						vert1.texcoord = glm::vec2(V1[0], V1[1]);
						vert2.texcoord = glm::vec2(V2[0], V2[1]);
						vert3.texcoord = glm::vec2(V3[0], V3[1]);
					}

					
					mesh.m_Vertex.push_back(vert1);
					mesh.m_Vertex.push_back(vert2);
					mesh.m_Vertex.push_back(vert3);
					nVertex += 3;
				}
			}

			glGenVertexArrays(1, &mesh.VAO);
			glGenBuffers(1, &mesh.VBO);

			for (int i = 0; i < deformerCount; ++i)
			{
				FbxDeformer* deformer = pMesh->GetDeformer(i);
				if (deformer == NULL) continue;
				// 只考虑eSKIN的管理方式
				if (deformer->GetDeformerType() != FbxDeformer::eSkin)
					continue;

				FbxSkin* pSkin = (FbxSkin*)(deformer);
				if (pSkin == NULL) continue;

				int clusterCount = pSkin->GetClusterCount();
				for (int j = 0; j < clusterCount; ++j)
				{
					FbxCluster* pCluster = pSkin->GetCluster(j);
					if (!pCluster) continue;
					auto pLinkNode = pCluster->GetLink();
					if (!pLinkNode)	continue;
					auto type = pLinkNode->GetNodeAttribute()->GetAttributeType();
					if (type != FbxNodeAttribute::eSkeleton) continue;

					unsigned int id = pLinkNode->GetUniqueID();
					if (m_BonesInverse.find(id) == m_BonesInverse.end())
					{
						FbxAMatrix transformLinkMatrix, resulttransformMatrix;
						pCluster->GetTransformLinkMatrix(transformLinkMatrix);
						resulttransformMatrix = transformLinkMatrix.Inverse();
						glm::mat4 resultMat;
						for (int m = 0; m < 4; ++m)
						{
							for (int n = 0; n < 4; ++n)
							{
								resultMat[m][n] = resulttransformMatrix[m][n];
							}
						}
						m_BonesInverse.insert(std::make_pair(id, resultMat));
					}
					auto mode = pCluster->GetLinkMode();
					int ControlPointIndicesCount = pCluster->GetControlPointIndicesCount();
					int* ControlPointIndices = pCluster->GetControlPointIndices();
					double* ControlPointWeights = pCluster->GetControlPointWeights();
					if (ControlPointIndicesCount > 0)
					{
						FbxAMatrix transformMatrix;
						pCluster->GetTransformMatrix(transformMatrix);
						transformMatrix = transformMatrix * geometricMat;
						for (int m = 0; m < 4; ++m)
						{
							for (int n = 0; n < 4; ++n)
							{
								mesh.m_linkMat[m][n] = transformMatrix[m][n];
							}
						}
					}
					for (int k = 0; k < ControlPointIndicesCount; ++k)
					{
						int cpi = ControlPointIndices[k];
						for (auto&x : mesh.m_Vertex)
						{
							if (x.index == cpi)
							{
								for (int index = 0; index < NUM_BONES_PER_VEREX; ++index)
								{
									if (index < 4)
									{
										if (x.bonesID[index] < 0)
										{
											x.bonesID[index] = id;
											x.bonesWeight[index] = ControlPointWeights[k];
											break;
										}
									}
									else if (index < 8)
									{
										if (x.bonesID2[index - 4] < 0)
										{
											x.bonesID2[index - 4] = id;
											x.bonesWeight2[index - 4] = ControlPointWeights[k];
											break;
										}
									}
									else
									{
										if (x.bonesID3[index - 8] < 0)
										{
											x.bonesID3[index - 8] = id;
											x.bonesWeight3[index - 8] = ControlPointWeights[k];
											break;
										}
									}
								}
							}
						}
					}
				}
			}

			//m_MeshVec.push_back(mesh);
		}
		break;
		default:
			break;
		}
	}
}

StateSetContent convert(const FbxSurfaceMaterial* pFbxMat)
{
	FbxMaterialMap::const_iterator it = _fbxMaterialMap.find(pFbxMat);
	if (it != _fbxMaterialMap.end())
		return it->second;
	StateSetContent result;

	FbxString shadingModel = pFbxMat->ShadingModel.Get();

	const FbxSurfaceLambert* pFbxLambert = FbxCast<FbxSurfaceLambert>(pFbxMat);

	// diffuse map...
	const FbxProperty lProperty = pFbxMat->FindProperty(FbxSurfaceMaterial::sDiffuse);
	if (lProperty.IsValid())
	{
		int lNbTex = lProperty.GetSrcObjectCount<FbxFileTexture>();
		for (int lTextureIndex = 0; lTextureIndex < lNbTex; lTextureIndex++)
		{
			FbxFileTexture* lTexture = FbxCast<FbxFileTexture>(lProperty.GetSrcObject<FbxFileTexture>(lTextureIndex));
			if (lTexture)
			{
				result.diffuseTextureName = lTexture->GetFileName();
				result.diffuseChannel = lTexture->UVSet.Get();
				result.diffuseScaleU = lTexture->GetScaleU();
				result.diffuseScaleV = lTexture->GetScaleV();
			}

			//For now only allow 1 texture
			break;
		}
	}

	double transparencyColorFactor = 1.0;
	bool useTransparencyColorFactor = false;

	// opacity map...
	const FbxProperty lOpacityProperty = pFbxMat->FindProperty(FbxSurfaceMaterial::sTransparentColor);
	if (lOpacityProperty.IsValid())
	{
		FbxDouble3 transparentColor = lOpacityProperty.Get<FbxDouble3>();
		// If transparent color is defined set the transparentFactor to gray scale value of transparentColor
		if (transparentColor[0] < 1.0 || transparentColor[1] < 1.0 || transparentColor[2] < 1.0) {
			transparencyColorFactor = transparentColor[0] * 0.30 + transparentColor[1] * 0.59 + transparentColor[2] * 0.11;
			useTransparencyColorFactor = true;
		}

		int lNbTex = lOpacityProperty.GetSrcObjectCount<FbxFileTexture>();
		for (int lTextureIndex = 0; lTextureIndex < lNbTex; lTextureIndex++)
		{
			FbxFileTexture* lTexture = FbxCast<FbxFileTexture>(lOpacityProperty.GetSrcObject<FbxFileTexture>(lTextureIndex));
			if (lTexture)
			{
				result.opacityTextureName = lTexture->GetFileName();
				result.opacityChannel = lTexture->UVSet.Get();
				result.opacityScaleU = lTexture->GetScaleU();
				result.opacityScaleV = lTexture->GetScaleV();
			}

			//For now only allow 1 texture
			break;
		}
	}

	// reflection map...
	const FbxProperty lReflectionProperty = pFbxMat->FindProperty(FbxSurfaceMaterial::sReflection);
	if (lReflectionProperty.IsValid())
	{
		int lNbTex = lReflectionProperty.GetSrcObjectCount<FbxFileTexture>();
		for (int lTextureIndex = 0; lTextureIndex < lNbTex; lTextureIndex++)
		{
			FbxFileTexture* lTexture = FbxCast<FbxFileTexture>(lReflectionProperty.GetSrcObject<FbxFileTexture>(lTextureIndex));
			if (lTexture)
			{
				// support only spherical reflection maps...
				if (FbxFileTexture::eUMT_ENVIRONMENT == lTexture->CurrentMappingType.Get())
				{
					result.reflectionTextureName = lTexture->GetFileName();
					result.reflectionChannel = lTexture->UVSet.Get();
				}
			}
			//For now only allow 1 texture
			break;
		}
	}

	// emissive map...
	const FbxProperty lEmissiveProperty = pFbxMat->FindProperty(FbxSurfaceMaterial::sEmissive);
	if (lEmissiveProperty.IsValid())
	{
		int lNbTex = lEmissiveProperty.GetSrcObjectCount<FbxFileTexture>();
		for (int lTextureIndex = 0; lTextureIndex < lNbTex; lTextureIndex++)
		{
			FbxFileTexture* lTexture = FbxCast<FbxFileTexture>(lEmissiveProperty.GetSrcObject<FbxFileTexture>(lTextureIndex));
			if (lTexture)
			{
				result.emissiveTextureName = lTexture->GetFileName();
				result.emissiveChannel = lTexture->UVSet.Get();
				result.emissiveScaleU = lTexture->GetScaleU();
				result.emissiveScaleV = lTexture->GetScaleV();
			}

			//For now only allow 1 texture
			break;
		}
	}

	// ambient map...
	const FbxProperty lAmbientProperty = pFbxMat->FindProperty(FbxSurfaceMaterial::sAmbient);
	if (lAmbientProperty.IsValid())
	{
		int lNbTex = lAmbientProperty.GetSrcObjectCount<FbxFileTexture>();
		for (int lTextureIndex = 0; lTextureIndex < lNbTex; lTextureIndex++)
		{
			FbxFileTexture* lTexture = FbxCast<FbxFileTexture>(lAmbientProperty.GetSrcObject<FbxFileTexture>(lTextureIndex));
			if (lTexture)
			{
				result.ambientTextureName = lTexture->GetFileName();
				result.ambientChannel = lTexture->UVSet.Get();
				result.ambientScaleU = lTexture->GetScaleU();
				result.ambientScaleV = lTexture->GetScaleV();
			}

			//For now only allow 1 texture
			break;
		}
	}

	if (pFbxLambert)
	{
		FbxDouble3 color = pFbxLambert->Diffuse.Get();
		double factor = pFbxLambert->DiffuseFactor.Get();
		double transparencyFactor = useTransparencyColorFactor ? transparencyColorFactor : pFbxLambert->TransparencyFactor.Get();

		color = pFbxLambert->Ambient.Get();
		factor = pFbxLambert->AmbientFactor.Get();

		color = pFbxLambert->Emissive.Get();
		factor = pFbxLambert->EmissiveFactor.Get();

		// get maps factors... 
		result.diffuseFactor = pFbxLambert->DiffuseFactor.Get();

		if (const FbxSurfacePhong* pFbxPhong = FbxCast<FbxSurfacePhong>(pFbxLambert))
		{
			color = pFbxPhong->Specular.Get();
			factor = pFbxPhong->SpecularFactor.Get();
			// Since Maya and 3D studio Max stores their glossiness values in exponential format (2^(log2(x)) 
			// We need to linearize to values between 0-100 and then scale to values between 0-128.
			// Glossiness values above 100 will result in shininess larger than 128.0 and will be clamped
			double shininess = (64.0 * log(pFbxPhong->Shininess.Get())) / (5.0 * log(2.0));

			// get maps factors...
			result.reflectionFactor = pFbxPhong->ReflectionFactor.Get();
			// get more factors here...
		}
	}

	_fbxMaterialMap.insert(FbxMaterialMap::value_type(pFbxMat, result));

	return result;
}

// scans StateSetList looking for the (first) channel name for the specified map type...
std::string getUVChannelForTextureMap(std::vector<StateSetContent>& stateSetList, const char* pName)
{
	// will return the first occurrence in the state set list...
	// TODO: what if more than one channel for the same map type?
	for (unsigned int i = 0; i < stateSetList.size(); i++)
	{
		if (0 == strcmp(pName, FbxSurfaceMaterial::sDiffuse))
			return stateSetList[i].diffuseChannel;
		if (0 == strcmp(pName, FbxSurfaceMaterial::sTransparentColor))
			return stateSetList[i].opacityChannel;
		if (0 == strcmp(pName, FbxSurfaceMaterial::sReflection))
			return stateSetList[i].reflectionChannel;
		if (0 == strcmp(pName, FbxSurfaceMaterial::sEmissive))
			return stateSetList[i].emissiveChannel;
		if (0 == strcmp(pName, FbxSurfaceMaterial::sAmbient))
			return stateSetList[i].ambientChannel;
		// more here...
	}

	return "";
}

// scans mesh layers looking for the UV element corresponding to the specified channel name...
const FbxLayerElementUV* getUVElementForChannel(std::string uvChannelName,
	FbxLayerElement::EType elementType, FbxMesh* pFbxMesh)
{
	// scan layers for specified UV channel...
	for (int cLayerIndex = 0; cLayerIndex < pFbxMesh->GetLayerCount(); cLayerIndex++)
	{
		const FbxLayer* pFbxLayer = pFbxMesh->GetLayer(cLayerIndex);
		if (!pFbxLayer)
			continue;

		if (const FbxLayerElementUV* uv = pFbxLayer->GetUVs())
		{
			if (0 == uvChannelName.compare(uv->GetName()))
				return uv;
		}
	}

	for (int cLayerIndex = 0; cLayerIndex < pFbxMesh->GetLayerCount(); cLayerIndex++)
	{
		const FbxLayer* pFbxLayer = pFbxMesh->GetLayer(cLayerIndex);
		if (!pFbxLayer)
			continue;

		if (const FbxLayerElementUV* uv = pFbxLayer->GetUVs(elementType))
		{
			return uv;
		}
	}

	return 0;
}

template <typename T>
bool layerElementValid(const FbxLayerElementTemplate<T>* pLayerElement)
{
	if (!pLayerElement)
		return false;

	switch (pLayerElement->GetMappingMode())
	{
	case FbxLayerElement::eByControlPoint:
	case FbxLayerElement::eByPolygonVertex:
	case FbxLayerElement::eByPolygon:
		break;
	default:
		return false;
	}

	switch (pLayerElement->GetReferenceMode())
	{
	case FbxLayerElement::eDirect:
	case FbxLayerElement::eIndexToDirect:
		return true;
	default:
		break;
	}

	return false;
}

template <typename FbxT>
FbxT getElement(const FbxLayerElementTemplate<FbxT>* pLayerElement,
	const FbxMesh* fbxMesh,
	int nPolygon, int nPolyVertex, int nMeshVertex)
{
	return pLayerElement->GetDirectArray().GetAt(getVertexIndex(
		pLayerElement, fbxMesh, nPolygon, nPolyVertex, nMeshVertex));
}

template <typename T>
int getVertexIndex(const FbxLayerElementTemplate<T>* pLayerElement,
	const FbxMesh* fbxMesh,
	int nPolygon, int nPolyVertex, int nMeshVertex)
{
	int index = 0;

	switch (pLayerElement->GetMappingMode())
	{
	case FbxLayerElement::eByControlPoint:
		index = fbxMesh->GetPolygonVertex(nPolygon, nPolyVertex);
		break;
	case FbxLayerElement::eByPolygonVertex:
		index = nMeshVertex;
		break;
	case FbxLayerElement::eByPolygon:
		index = nPolygon;
		break;
	default:
		;// OSG_WARN << "getVertexIndex: unsupported FBX mapping mode" << std::endl;
	}

	if (pLayerElement->GetReferenceMode() == FbxLayerElement::eDirect)
	{
		return index;
	}

	return pLayerElement->GetIndexArray().GetAt(index);
}

//utf8转ansi 支持0SG中文
std::string  _u8ToAnsi(const std::string & str)
{
	int nwLen = MultiByteToWideChar(CP_UTF8, 0, str.c_str(), -1, NULL, 0);
	wchar_t * pwBuf = new wchar_t[nwLen + 1];//一定要加1，不然会出现尾巴
	memset(pwBuf, 0, nwLen * 2 + 2);
	MultiByteToWideChar(CP_UTF8, 0, str.c_str(), str.length(), pwBuf, nwLen);
	int nLen = WideCharToMultiByte(CP_ACP, 0, pwBuf, -1, NULL, NULL, NULL, NULL);
	char * pBuf = new char[nLen + 1];
	memset(pBuf, 0, nLen + 1);
	WideCharToMultiByte(CP_ACP, 0, pwBuf, nwLen, pBuf, nLen, NULL, NULL);
	std::string retStr = pBuf;
	delete[]pBuf;
	delete[]pwBuf;
	pBuf = NULL;
	pwBuf = NULL;
	return retStr;
}

std::string string_To_UTF8(const std::string & str)
{
	int nwLen = ::MultiByteToWideChar(CP_ACP, 0, str.c_str(), -1, NULL, 0);
	wchar_t * pwBuf = new wchar_t[nwLen + 1];//一定要加1，不然会出现尾巴 
	ZeroMemory(pwBuf, nwLen * 2 + 2);
	::MultiByteToWideChar(CP_ACP, 0, str.c_str(), str.length(), pwBuf, nwLen);
	int nLen = ::WideCharToMultiByte(CP_UTF8, 0, pwBuf, -1, NULL, NULL, NULL, NULL);
	char * pBuf = new char[nLen + 1];
	ZeroMemory(pBuf, nLen + 1);
	::WideCharToMultiByte(CP_UTF8, 0, pwBuf, nwLen, pBuf, nLen, NULL, NULL);
	std::string retStr(pBuf);
	delete[]pwBuf;
	delete[]pBuf;
	pwBuf = NULL;
	pBuf = NULL;
	return retStr;
}
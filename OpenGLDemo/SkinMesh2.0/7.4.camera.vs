#version 450 core

const int BONES_COUNT = 100;

layout (location = 0) in vec3 aPos;
layout (location = 1) in vec3 aNormal;
layout (location = 2) in vec2 aTexCoord;
layout (location = 3) in ivec4 bonesIndexInFrames;
layout (location = 4) in vec4 bonesWeight;

layout (location = 5) in ivec4 bonesIndexInFrames2;
layout (location = 6) in vec4 bonesWeight2;
layout (location = 7) in ivec4 bonesIndexInFrames3;
layout (location = 8) in vec4 bonesWeight3;

out vec2 TexCoord;
out vec4 FragPos;
out vec3 Normal;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;
uniform bool hasBone;
uniform mat4 bonesMat[BONES_COUNT];

mat4 ComputeVertexByBoneMatrix();

void main()
{
	vec4 pos = vec4(aPos, 1.0);
	FragPos = model * pos;
	Normal = mat3(transpose(inverse(model))) * aNormal;  

	if(hasBone)
	{
		mat4 trsMat = ComputeVertexByBoneMatrix();
		FragPos = trsMat * pos;
		Normal = mat3(transpose(inverse(trsMat))) * aNormal; 
	} 
	
	gl_Position = projection * view * FragPos;
	TexCoord = vec2(aTexCoord.x, aTexCoord.y);
}


mat4 ComputeVertexByBoneMatrix()
{
	if(bonesIndexInFrames.x == 0 && bonesWeight.x == 0)
		return model;

	mat4 matTrans =  bonesMat[bonesIndexInFrames.x] *  bonesWeight.x;
	matTrans += bonesMat[bonesIndexInFrames.y] *  bonesWeight.y;
	matTrans += bonesMat[bonesIndexInFrames.z] *  bonesWeight.z;
	matTrans += bonesMat[bonesIndexInFrames.w] *  bonesWeight.w;

	matTrans += bonesMat[bonesIndexInFrames2.x] *  bonesWeight2.x;
	matTrans += bonesMat[bonesIndexInFrames2.y] *  bonesWeight2.y;
	matTrans += bonesMat[bonesIndexInFrames2.z] *  bonesWeight2.z;
	matTrans += bonesMat[bonesIndexInFrames2.w] *  bonesWeight2.w;

	matTrans += bonesMat[bonesIndexInFrames3.x] *  bonesWeight3.x;
	matTrans += bonesMat[bonesIndexInFrames3.y] *  bonesWeight3.y;
	matTrans += bonesMat[bonesIndexInFrames3.z] *  bonesWeight3.z;
	matTrans += bonesMat[bonesIndexInFrames3.w] *  bonesWeight3.w;
	
	return matTrans;
}
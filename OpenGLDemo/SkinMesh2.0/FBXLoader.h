#pragma once
struct Mesh;
class FBXLoader
{
public:
	FBXLoader();
	virtual ~FBXLoader();
	bool Init();
	bool LoadModel(const char* pFilename);
	void ReadNode(FbxNode* pNode);
	const std::vector<Mesh>& getMeshs();
	SkeletonAnimation& getAnimation() { return m_Animation; };

protected:
	FbxManager* m_fbxMgr;
	FbxScene* m_scene;
	unsigned int m_numStacks;

	std::vector<Mesh> m_MeshVec;
	SkeletonAnimation m_Animation;

	std::map<unsigned int, glm::mat4> m_BonesInverse;
};
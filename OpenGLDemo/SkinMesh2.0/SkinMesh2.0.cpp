// SkinMesh2.0.cpp : 定义控制台应用程序的入口点。
//

#include "stdafx.h"
#include <iostream>

#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <stb_image.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "shader_m.h"
#include "camera.h"

#include <fbxsdk.h>
#include "ModelData.h"
#include "FBXLoader.h"

void framebuffer_size_callback(GLFWwindow* window, int width, int height);
void mouse_callback(GLFWwindow* window, double xpos, double ypos);
void scroll_callback(GLFWwindow* window, double xoffset, double yoffset);
void processInput(GLFWwindow *window);
int InitOpenGL(GLFWwindow*& window);

// settings
const unsigned int SCR_WIDTH = 1920;
const unsigned int SCR_HEIGHT = 1080;

// camera
Camera camera(glm::vec3(0.0f, 0.0f, 30.0f));
float lastX = SCR_WIDTH / 2.0f;
float lastY = SCR_HEIGHT / 2.0f;
bool firstMouse = true;

// timing
float deltaTime = 0.0f;	// time between current frame and last frame
float lastFrame = 0.0f;

SkeletonAnimation* pAnimation = 0;

int main()
{
	GLFWwindow* window = 0;
	if (-1 == InitOpenGL(window))
		return -1;

	// configure global opengl state
	// -----------------------------
	glEnable(GL_DEPTH_TEST);
	Shader ourShader("7.4.camera.vs", "7.4.camera.fs");

	FBXLoader fbxloader;
	fbxloader.Init();
	//bool isLoadSuccess = fbxloader.LoadModel("D:\\road1.FBX");
	//bool isLoadSuccess = fbxloader.LoadModel("D:\\hong kong park_simplified_3d_mesh.fbx");
	//bool isLoadSuccess = fbxloader.LoadModel("D:\\standup2.FBX");//yizhu4Ani2.FBX
	bool isLoadSuccess = fbxloader.LoadModel("D:\\Padoru_v1-4.Fbx");//humanoid
	if (!isLoadSuccess) return -1;

	const std::vector<Mesh>& meshs = fbxloader.getMeshs();
	SkeletonAnimation& animation = fbxloader.getAnimation();
	animation.setCurrentAnimation(0);
	animation.setPlayMode(SkeletonAnimation::LOOP);
	pAnimation = &animation;

	for (int i = 0; i < meshs.size(); ++i)
	{
		const Mesh& mesh = meshs[i];
		if (mesh.m_Vertex.empty())
		{
			continue;
		}

		glBindVertexArray(mesh.VAO);
		glBindBuffer(GL_ARRAY_BUFFER, mesh.VBO);
		glBufferData(GL_ARRAY_BUFFER, mesh.m_Vertex.size() * sizeof(Vertex), &mesh.m_Vertex[0], GL_STATIC_DRAW);

		// position attribute
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)0);
		glEnableVertexAttribArray(0);

		glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, normal));
		glEnableVertexAttribArray(1);

		glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, texcoord));
		glEnableVertexAttribArray(2);

		glVertexAttribIPointer(3, 4, GL_INT, sizeof(Vertex), (void*)offsetof(Vertex, bonesIndexInFrames));
		glEnableVertexAttribArray(3);

		glVertexAttribPointer(4, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, bonesWeight));
		glEnableVertexAttribArray(4);

		glVertexAttribIPointer(5, 4, GL_INT, sizeof(Vertex), (void*)offsetof(Vertex, bonesIndexInFrames2));
		glEnableVertexAttribArray(5);

		glVertexAttribPointer(6, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, bonesWeight2));
		glEnableVertexAttribArray(6);

		glVertexAttribIPointer(7, 4, GL_INT, sizeof(Vertex), (void*)offsetof(Vertex, bonesIndexInFrames3));
		glEnableVertexAttribArray(7);

		glVertexAttribPointer(8, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, bonesWeight3));
		glEnableVertexAttribArray(8);
	}

	// tell opengl for each sampler to which texture unit it belongs to (only has to be done once)
	// -------------------------------------------------------------------------------------------
	//Shader ourShader("7.4.camera.vs", "7.4.camera.fs");
	ourShader.use();
	ourShader.setInt("texture1", 0);

	if (0 == animation.getAnimStacksCount() || 0 == animation.getCurrentChannelsCount())
	{
		float tx = meshs[0].m_worldMat[3][0];
		float ty = meshs[0].m_worldMat[3][1];
		float tz = meshs[0].m_worldMat[3][2];
		camera.Position = meshs[0].m_Vertex[0].pos + glm::vec3(tx, ty, tz);
	}
	else
	{
		unsigned int id = animation.getRootBonePostures().begin()->first;
		glm::mat4 mat = animation.getRootBonePostures().begin()->second;
		mat = mat * animation.getSkeleton()._bones[id].currentPosture.localMat
			* animation.getSkeleton()._bones[id].geometricMat
			* animation.getSkeleton()._bones[id].vertexsToBoneMat;
		camera.Position = mat * meshs[0].m_linkMat * glm::vec4(meshs[0].m_Vertex[0].pos, 1.0);
	}

	// render loop
	// -----------
	while (!glfwWindowShouldClose(window))
	{
		// per-frame time logic
		// --------------------
		float currentFrame = glfwGetTime();
		deltaTime = currentFrame - lastFrame;
		lastFrame = currentFrame;

		// input
		// -----
		processInput(window);

		// render
		// ------
		glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		// activate shader
		ourShader.use();

		// pass projection matrix to shader (note that in this case it could change every frame)
		glm::mat4 projection = glm::perspective(glm::radians(camera.Zoom), (float)SCR_WIDTH / (float)SCR_HEIGHT, 0.1f, 100000.0f);
		ourShader.setMat4("projection", projection);

		// camera/view transformation
		glm::mat4 view = camera.GetViewMatrix();
		ourShader.setMat4("view", view);

		for (int i = 0; i < meshs.size(); ++i)
		{
			const Mesh& mesh = meshs[i];
			if (mesh.m_Vertex.empty())
			{
				continue;
			}
			glActiveTexture(GL_TEXTURE0);
			glBindTexture(GL_TEXTURE_2D, mesh.texID);

			if (0 == mesh.texID)
				ourShader.setInt("type", 0);
			else
				ourShader.setInt("type", 1);

			if (0 != animation.getAnimStacksCount() && 0 != animation.getCurrentChannelsCount())
			{
				ourShader.setBool("hasBone", true);

				animation.update(currentFrame);
				std::vector<glm::mat4> matVec = animation.getSkeletonPostures();
				for (auto&x : matVec)
				{
					x = x * mesh.m_linkMat;
				}

				ourShader.setMat4List("bonesMat", matVec.size(), &matVec[0]);
				const glm::mat4& model = mesh.m_linkMat;
				ourShader.setMat4("model", model);
			}
			else
			{
				ourShader.setBool("hasBone", false);
				const glm::mat4& model = mesh.m_worldMat;
				ourShader.setMat4("model", model);
			}

			glBindVertexArray(mesh.VAO);

			//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
			//glDrawArrays(GL_TRIANGLES, 0, mesh.m_Vertex.size());

			glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
			glDrawArrays(GL_TRIANGLES, 0, mesh.m_Vertex.size());
		}

		// glfw: swap buffers and poll IO events (keys pressed/released, mouse moved etc.)
		// -------------------------------------------------------------------------------
		glfwSwapBuffers(window);
		glfwPollEvents();
	}

	// optional: de-allocate all resources once they've outlived their purpose:
	// ------------------------------------------------------------------------
	//glDeleteVertexArrays(1, &VAO);
	//glDeleteBuffers(1, &VBO);

	// glfw: terminate, clearing all previously allocated GLFW resources.
	// ------------------------------------------------------------------
	glfwTerminate();
	return 0;
}

int InitOpenGL(GLFWwindow*& window)
{
	// glfw: initialize and configure
	// ------------------------------
	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 5);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

#ifdef __APPLE__
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
#endif

	// glfw window creation
	// --------------------
	window = glfwCreateWindow(SCR_WIDTH, SCR_HEIGHT, "LearnOpenGL", NULL, NULL);
	if (window == NULL)
	{
		std::cout << "Failed to create GLFW window" << std::endl;
		glfwTerminate();
		return -1;
	}
	glfwMakeContextCurrent(window);
	glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);
	glfwSetCursorPosCallback(window, mouse_callback);
	glfwSetScrollCallback(window, scroll_callback);

	// tell GLFW to capture our mouse
	glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

	// glad: load all OpenGL function pointers
	// ---------------------------------------
	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
	{
		std::cout << "Failed to initialize GLAD" << std::endl;
		return -1;
	}

	return 0;
}



// process all input: query GLFW whether relevant keys are pressed/released this frame and react accordingly
// ---------------------------------------------------------------------------------------------------------
void processInput(GLFWwindow *window)
{
	if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
		glfwSetWindowShouldClose(window, true);

	if (pAnimation)
	{
		if (glfwGetKey(window, GLFW_KEY_P) == GLFW_PRESS)
		{
			if (pAnimation->isPause())		
				pAnimation->Pause(false);			
			else
				pAnimation->Pause(true);
		}
		if (glfwGetKey(window, GLFW_KEY_ENTER) == GLFW_PRESS)
		{
			unsigned int index = pAnimation->getCurrentAnimation();
			unsigned int count = pAnimation->getAnimStacksCount();
			if (index + 1 != count)
				++index;
			else
				index = 0;
			pAnimation->setCurrentAnimation(index);
		}
	}

	if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
		camera.ProcessKeyboard(FORWARD, deltaTime);
	if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
		camera.ProcessKeyboard(BACKWARD, deltaTime);
	if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
		camera.ProcessKeyboard(LEFT, deltaTime);
	if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
		camera.ProcessKeyboard(RIGHT, deltaTime);
}

// glfw: whenever the window size changed (by OS or user resize) this callback function executes
// ---------------------------------------------------------------------------------------------
void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
	// make sure the viewport matches the new window dimensions; note that width and 
	// height will be significantly larger than specified on retina displays.
	glViewport(0, 0, width, height);
}


// glfw: whenever the mouse moves, this callback is called
// -------------------------------------------------------
void mouse_callback(GLFWwindow* window, double xpos, double ypos)
{
	if (firstMouse)
	{
		lastX = xpos;
		lastY = ypos;
		firstMouse = false;
	}

	float xoffset = xpos - lastX;
	float yoffset = lastY - ypos; // reversed since y-coordinates go from bottom to top

	lastX = xpos;
	lastY = ypos;

	camera.ProcessMouseMovement(xoffset, yoffset);
}

// glfw: whenever the mouse scroll wheel scrolls, this callback is called
// ----------------------------------------------------------------------
void scroll_callback(GLFWwindow* window, double xoffset, double yoffset)
{
	camera.ProcessMouseScroll(yoffset);
}

// OpenGLBonesAnimation.cpp : 定义控制台应用程序的入口点。
//

#include "stdafx.h"
#include <iostream>
#include <glad/glad.h>
#include <GLFW/glfw3.h>
//#include <stb_image.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "shader_m.h"
#include "camera.h"

#include <fbxsdk.h>

void framebuffer_size_callback(GLFWwindow* window, int width, int height);
void mouse_callback(GLFWwindow* window, double xpos, double ypos);
void scroll_callback(GLFWwindow* window, double xoffset, double yoffset);
void processInput(GLFWwindow *window);

// settings
const unsigned int SCR_WIDTH = 1920;
const unsigned int SCR_HEIGHT = 1080;

// camera
Camera camera(glm::vec3(0.0f, 0.0f, 10.0f));
float lastX = SCR_WIDTH / 2.0f;
float lastY = SCR_HEIGHT / 2.0f;
bool firstMouse = true;

// timing
float deltaTime = 0.0f;	// time between current frame and last frame
float lastFrame = 0.0f;

float angleY = 0.0f;
float angleZ = 0.0f;

struct Vertex
{
	//! 位置
	float x, y, z;
	//! 颜色
	float r, g, b, a;
	//! 影响度
	float weights[2];
	//! 矩阵的索引
	short matrixIndices[2];
	//! 影响整个定点的骨头个数
	short numBones;
};

int main()
{
	// glfw: initialize and configure
	// ------------------------------
	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 5);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

#ifdef __APPLE__
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
#endif

	// glfw window creation
	// --------------------
	GLFWwindow* window = glfwCreateWindow(SCR_WIDTH, SCR_HEIGHT, "LearnOpenGL", NULL, NULL);
	if (window == NULL)
	{
		std::cout << "Failed to create GLFW window" << std::endl;
		glfwTerminate();
		return -1;
	}
	glfwMakeContextCurrent(window);
	glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);
	glfwSetCursorPosCallback(window, mouse_callback);
	glfwSetScrollCallback(window, scroll_callback);

	// tell GLFW to capture our mouse
	glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

	// glad: load all OpenGL function pointers
	// ---------------------------------------
	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
	{
		std::cout << "Failed to initialize GLAD" << std::endl;
		return -1;
	}

	// configure global opengl state
	// -----------------------------
	glEnable(GL_DEPTH_TEST);

	// build and compile our shader zprogram
	// ------------------------------------
	Shader ourShader("7.4.camera.vs", "7.4.camera.fs");

	// set up vertex data (and buffer(s)) and configure vertex attributes
	// ------------------------------------------------------------------
	Vertex vertices[] = {
		{	-1.0f,0.0f,0.0f,  1.0f,1.0f,0.0f,1.0f,   1.0f,0.0f,  0,0,  1 }, // 蓝色
		{	1.0f,0.0f,0.0f,	  1.0f,1.0f,0.0f,1.0f,   1.0f,0.0f,  0,0,  1 },
		{	1.0f,2.0f,0.0f,   1.0f,1.0f,0.0f,1.0f,   0.5f,0.5f,  0,1,  2 },
		{	-1.0f,2.0f,0.0f,  1.0f,1.0f,0.0f,1.0f,   0.5f,0.5f,  0,1,  2 },

		{	-1.0f,2.0f,0.0f,  0.0f,1.0f,0.0f,1.0f,   0.5f,0.5f,  0,1,  2 }, // 绿色
		{	1.0f,2.0f,0.0f,   0.0f,1.0f,0.0f,1.0f,   0.5f,0.5f,  0,1,  2 },
		{	1.0f,4.0f,0.0f,   0.0f,1.0f,0.0f,1.0f,   0.5f,0.5f,  0,1,  2 },
		{	-1.0f,4.0f,0.0f,  0.0f,1.0f,0.0f,1.0f,   0.5f,0.5f,  0,1,  2 },

		{	-1.0f,4.0f,0.0f,  0.0f,0.0f,1.0f,1.0f,   0.5f,0.5f,  0,1,  2 }, // 黄色
		{	1.0f,4.0f,0.0f,   0.0f,0.0f,1.0f,1.0f,   0.5f,0.5f,  0,1,  2 },
		{	1.0f,6.0f,0.0f,   0.0f,0.0f,1.0f,1.0f,   1.0f,0.0f,  1,0,  1 },
		{	-1.0f,6.0f,0.0f,  0.0f,0.0f,1.0f,1.0f,   1.0f,0.0f,  1,0,  1 }
	};

	float   arBone[] =
	{
		0.0f, 0.0f, 0.0f,
		-0.2f, 0.2f,-0.2f,
		0.2f, 0.2f,-0.2f,
		0.0f, 3.0f, 0.0f,
		-0.2f, 0.2f,-0.2f,
		-0.2f, 0.2f, 0.2f,
		0.0f, 0.0f, 0.0f,
		0.2f, 0.2f,-0.2f,
		0.2f, 0.2f, 0.2f,
		0.0f, 0.0f, 0.0f,
		-0.2f, 0.2f, 0.2f,
		0.0f, 3.0f, 0.0f,
		0.2f, 0.2f, 0.2f,
		-0.2f, 0.2f, 0.2f,
	};

	glm::mat4 g_boneMatrix[2];
	glm::mat4 g_matrixToRenderBone[2];
	g_boneMatrix[0] = glm::mat4(1.0f);
	g_matrixToRenderBone[0] = glm::mat4(1.0f);

	unsigned int VBOs[2], VAOs[2];
	glGenVertexArrays(2, VAOs);
	glGenBuffers(2, VBOs);

	glBindVertexArray(VAOs[0]);
	glBindBuffer(GL_ARRAY_BUFFER, VBOs[0]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

	// position attribute
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)0);
	glEnableVertexAttribArray(0);
	// texture coord attribute
	glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)(3 * sizeof(float)));
	glEnableVertexAttribArray(1);

	glBindVertexArray(VAOs[1]);
	glBindBuffer(GL_ARRAY_BUFFER, VBOs[1]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(arBone), arBone, GL_STATIC_DRAW);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
	glEnableVertexAttribArray(0);

	// render loop
	// -----------
	while (!glfwWindowShouldClose(window))
	{
		// per-frame time logic
		// --------------------
		float currentFrame = glfwGetTime();
		deltaTime = currentFrame - lastFrame;
		lastFrame = currentFrame;

		// input
		// -----
		processInput(window);

		// render
		// ------
		glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		// activate shader
		ourShader.use();

		// pass projection matrix to shader (note that in this case it could change every frame)
		glm::mat4 projection = glm::perspective(glm::radians(camera.Zoom), (float)SCR_WIDTH / (float)SCR_HEIGHT, 0.1f, 100.0f);
		ourShader.setMat4("projection", projection);

		// camera/view transformation
		glm::mat4 view = camera.GetViewMatrix();
		ourShader.setMat4("view", view);

		glm::mat4 model = glm::mat4(1.0f);
		ourShader.setMat4("model", model);
		
		//蒙皮坐标
		Vertex  calQuadVertices[12];
		memcpy(calQuadVertices, vertices, sizeof(vertices));
		for (int i = 0; i < 12; ++i)
		{
			glm::vec4 vec(0, 0, 0, 1.0f);
			glm::vec4 vecSrc(vertices[i].x, vertices[i].y, vertices[i].z, 1.0f);
			for (int x = 0; x < calQuadVertices[i].numBones; ++x)
			{
				//! 计算位置
				glm::vec4 temp = g_boneMatrix[vertices[i].matrixIndices[x]] * vecSrc;
				//! 计算权重位置
				vec += temp * vertices[i].weights[x];
			}
			calQuadVertices[i].x = vec.x;
			calQuadVertices[i].y = vec.y;
			calQuadVertices[i].z = vec.z;
		}

		glBindVertexArray(VAOs[0]);
		glBindBuffer(GL_ARRAY_BUFFER, VBOs[0]);
		glBufferData(GL_ARRAY_BUFFER, sizeof(calQuadVertices), calQuadVertices, GL_STATIC_DRAW);

		// position attribute
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)0);
		glEnableVertexAttribArray(0);
		// texture coord attribute
		glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)(3 * sizeof(float)));
		glEnableVertexAttribArray(1);

		for (int i = 0; i < 3; ++i)
		{
			glDrawArrays(GL_LINE_LOOP, i * 4, 4);
		}


		glBindVertexArray(VAOs[1]);
		glm::mat4 boneRotationMatrix = glm::mat4(1.0f);
		boneRotationMatrix = glm::rotate(boneRotationMatrix, glm::radians(angleY), glm::vec3(0.0f, 1.0f, 0.0f));
		boneRotationMatrix = glm::rotate(boneRotationMatrix, glm::radians(angleZ), glm::vec3(0.0f, 0.0f, 1.0f));
		//骨骼的世界坐标矩阵
		g_matrixToRenderBone[1] = g_boneMatrix[0] * glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 3.0f, 0.0f)) * boneRotationMatrix;
		//点在对应骨骼中的局部坐标矩阵 * 骨骼的世界坐标矩阵
		g_boneMatrix[1] = g_boneMatrix[0] * glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 3.0f, 0.0f)) * boneRotationMatrix * glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, -3.0f, 0.0f));

		for (unsigned int i = 0; i < 2; i++)
		{
			glm::mat4 model = g_matrixToRenderBone[i];
			ourShader.setMat4("model", model);

			glDrawArrays(GL_LINE_STRIP, 0, sizeof(arBone) / 12);
		}

		// glfw: swap buffers and poll IO events (keys pressed/released, mouse moved etc.)
		// -------------------------------------------------------------------------------
		glfwSwapBuffers(window);
		glfwPollEvents();
	}

	// optional: de-allocate all resources once they've outlived their purpose:
	// ------------------------------------------------------------------------
	glDeleteVertexArrays(2, VAOs);
	glDeleteBuffers(2, VBOs);

	// glfw: terminate, clearing all previously allocated GLFW resources.
	// ------------------------------------------------------------------
	glfwTerminate();
	return 0;
}

// process all input: query GLFW whether relevant keys are pressed/released this frame and react accordingly
// ---------------------------------------------------------------------------------------------------------
void processInput(GLFWwindow *window)
{
	if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
		glfwSetWindowShouldClose(window, true);

	if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
		camera.ProcessKeyboard(FORWARD, deltaTime);
	if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
		camera.ProcessKeyboard(BACKWARD, deltaTime);
	if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
		camera.ProcessKeyboard(LEFT, deltaTime);
	if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
		camera.ProcessKeyboard(RIGHT, deltaTime);
	if (glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS)
		angleZ += 1.0f;
	if (glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS)
		angleZ -= 1.0f;
	if (glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS)
		angleY += 1.0f;
	if (glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_PRESS)
		angleY -= 1.0f;
}

// glfw: whenever the window size changed (by OS or user resize) this callback function executes
// ---------------------------------------------------------------------------------------------
void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
	// make sure the viewport matches the new window dimensions; note that width and 
	// height will be significantly larger than specified on retina displays.
	glViewport(0, 0, width, height);
}


// glfw: whenever the mouse moves, this callback is called
// -------------------------------------------------------
void mouse_callback(GLFWwindow* window, double xpos, double ypos)
{
	if (firstMouse)
	{
		lastX = xpos;
		lastY = ypos;
		firstMouse = false;
	}

	float xoffset = xpos - lastX;
	float yoffset = lastY - ypos; // reversed since y-coordinates go from bottom to top

	lastX = xpos;
	lastY = ypos;

	camera.ProcessMouseMovement(xoffset, yoffset);
}

// glfw: whenever the mouse scroll wheel scrolls, this callback is called
// ----------------------------------------------------------------------
void scroll_callback(GLFWwindow* window, double xoffset, double yoffset)
{
	camera.ProcessMouseScroll(yoffset);
}
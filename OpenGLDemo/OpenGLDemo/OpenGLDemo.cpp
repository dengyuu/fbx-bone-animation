// OpenGLDemo.cpp : 定义控制台应用程序的入口点。
//

#include "stdafx.h"
#include <iostream>

#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <stb_image.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "shader_m.h"
#include "camera.h"

#include <fbxsdk.h>
#include "ModelData.h"
#include "FBXLoader.h"

void framebuffer_size_callback(GLFWwindow* window, int width, int height);
void mouse_callback(GLFWwindow* window, double xpos, double ypos);
void scroll_callback(GLFWwindow* window, double xoffset, double yoffset);
void processInput(GLFWwindow *window);
void ComputerVertexs(std::vector<Vertex>& vertexs, const std::vector<BoneFrame>& boneFrames);
void showBones(const Shader& shader, const std::vector<BoneFrame>& boneframes);
glm::vec3 CalculateFaceNormal(const glm::vec3& originNormal, const glm::vec3& point1, const glm::vec3& point2, const glm::vec3& point3,
	const glm::vec3& newpoint1, const glm::vec3& newpoint2, const glm::vec3& newpoint3);

// settings
const unsigned int SCR_WIDTH = 1920;
const unsigned int SCR_HEIGHT = 1080;

// camera
Camera camera(glm::vec3(0.0f, 0.0f, 30.0f));
float lastX = SCR_WIDTH / 2.0f;
float lastY = SCR_HEIGHT / 2.0f;
bool firstMouse = true;

// timing
float deltaTime = 0.0f;	// time between current frame and last frame
float lastFrame = 0.0f;

bool bPause = false;
unsigned int itimes = 0;
int main()
{
	// glfw: initialize and configure
	// ------------------------------
	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 5);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

#ifdef __APPLE__
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
#endif

	// glfw window creation
	// --------------------
	GLFWwindow* window = glfwCreateWindow(SCR_WIDTH, SCR_HEIGHT, "LearnOpenGL", NULL, NULL);
	if (window == NULL)
	{
		std::cout << "Failed to create GLFW window" << std::endl;
		glfwTerminate();
		return -1;
	}
	glfwMakeContextCurrent(window);
	glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);
	glfwSetCursorPosCallback(window, mouse_callback);
	glfwSetScrollCallback(window, scroll_callback);

	// tell GLFW to capture our mouse
	glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

	// glad: load all OpenGL function pointers
	// ---------------------------------------
	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
	{
		std::cout << "Failed to initialize GLAD" << std::endl;
		return -1;
	}

	// configure global opengl state
	// -----------------------------
	glEnable(GL_DEPTH_TEST);

	// build and compile our shader zprogram
	// ------------------------------------
	Shader ourShader("7.4.camera.vs", "7.4.camera.fs");

	// set up vertex data (and buffer(s)) and configure vertex attributes
	// ------------------------------------------------------------------
	FBXLoader fbxloader;
	fbxloader.Init();
	//bool isLoadSuccess = fbxloader.LoadModel("D:\\road1.FBX");
	//bool isLoadSuccess = fbxloader.LoadModel("D:\\hong kong park_simplified_3d_mesh.fbx");
	//bool isLoadSuccess = fbxloader.LoadModel("D:\\yizhu4Ani2.FBX");//yizhu4Ani2
	bool isLoadSuccess = fbxloader.LoadModel("D:\\humanoid.fbx");//humanoid
	if (!isLoadSuccess) return -1;

	const std::vector<Mesh>& meshs = fbxloader.getMeshs();
	const std::vector<BoneFrame>& boneFrames = fbxloader.getBoneFrames();

	for (int i = 0; i < meshs.size(); ++i)
	{
		const Mesh& mesh = meshs[i];

		glBindVertexArray(mesh.VAO);
		glBindBuffer(GL_ARRAY_BUFFER, mesh.VBO);
		glBufferData(GL_ARRAY_BUFFER, mesh.m_Vertex.size() *sizeof(Vertex), &mesh.m_Vertex[0], GL_STATIC_DRAW);

		// position attribute
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)0);
		glEnableVertexAttribArray(0);

		glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, normal));
		glEnableVertexAttribArray(1);

		glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, texcoord));
		glEnableVertexAttribArray(2);
	}

	// tell opengl for each sampler to which texture unit it belongs to (only has to be done once)
	// -------------------------------------------------------------------------------------------
	ourShader.use();
	ourShader.setInt("texture1", 0);

	float tx = meshs[0].m_worldMat[3][0];
	float ty = meshs[0].m_worldMat[3][1];
	float tz = meshs[0].m_worldMat[3][2];
	camera.Position = meshs[0].m_Vertex[0].pos + glm::vec3(tx, ty, tz);

	// render loop
	// -----------
	while (!glfwWindowShouldClose(window))
	{
		// per-frame time logic
		// --------------------
		float currentFrame = glfwGetTime();
		deltaTime = currentFrame - lastFrame;
		lastFrame = currentFrame;
		if(!bPause) ++itimes;
		// input
		// -----
		processInput(window);

		// render
		// ------
		glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		// activate shader
		ourShader.use();

		// pass projection matrix to shader (note that in this case it could change every frame)
		glm::mat4 projection = glm::perspective(glm::radians(camera.Zoom), (float)SCR_WIDTH / (float)SCR_HEIGHT, 0.1f, 100000.0f);
		ourShader.setMat4("projection", projection);

		// camera/view transformation
		glm::mat4 view = camera.GetViewMatrix();
		ourShader.setMat4("view", view);

		//if (!boneFrames.empty())
		//	showBones(ourShader, boneFrames);

		for (int i = 0; i < meshs.size(); ++i)
		{
			const Mesh& mesh = meshs[i];
			glActiveTexture(GL_TEXTURE0);
			glBindTexture(GL_TEXTURE_2D, mesh.texID);

			if (0 == mesh.texID)
			{
				ourShader.setInt("type", 0);
			}
			else
			{
				ourShader.setInt("type", 1);
			}

			if (!boneFrames.empty())
			{
				glBindVertexArray(mesh.VAO);
				glBindBuffer(GL_ARRAY_BUFFER, mesh.VBO);

				std::vector<Vertex> vet = mesh.m_Vertex;
				ComputerVertexs(vet, boneFrames);
				glBufferData(GL_ARRAY_BUFFER, vet.size() * sizeof(Vertex), &vet[0], GL_STATIC_DRAW);

				glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)0);
				glEnableVertexAttribArray(0);

				glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, normal));
				glEnableVertexAttribArray(1);

				glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, texcoord));
				glEnableVertexAttribArray(2);

				//showBones(ourShader, boneFrames);
			}

			glBindVertexArray(mesh.VAO);
			glm::mat4 model = boneFrames.empty() ? mesh.m_worldMat : glm::mat4(1.0);
			ourShader.setMat4("model", model);

			//const MeshFrame& meshframe = meshFrames[i];
			//if (!meshframe.fpos.empty())
			//{
			//	unsigned int index = (int)currentFrame % meshframe.fpos.size();
			//	const glm::mat4& model = meshframe.fpos[index];
			//	ourShader.setMat4("model", model);
			//}

			//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
			//glDrawArrays(GL_TRIANGLES, 0, mesh.m_Vertex.size());

			glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
			glDrawArrays(GL_TRIANGLES, 0, mesh.m_Vertex.size());
		}

		// glfw: swap buffers and poll IO events (keys pressed/released, mouse moved etc.)
		// -------------------------------------------------------------------------------
		glfwSwapBuffers(window);
		glfwPollEvents();
	}

	// optional: de-allocate all resources once they've outlived their purpose:
	// ------------------------------------------------------------------------
	//glDeleteVertexArrays(1, &VAO);
	//glDeleteBuffers(1, &VBO);

	// glfw: terminate, clearing all previously allocated GLFW resources.
	// ------------------------------------------------------------------
	glfwTerminate();
	return 0;
}

// process all input: query GLFW whether relevant keys are pressed/released this frame and react accordingly
// ---------------------------------------------------------------------------------------------------------
void processInput(GLFWwindow *window)
{
	if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
		glfwSetWindowShouldClose(window, true);
	if (glfwGetKey(window, GLFW_KEY_SPACE) == GLFW_PRESS)
		bPause = !bPause;

	if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
		camera.ProcessKeyboard(FORWARD, deltaTime);
	if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
		camera.ProcessKeyboard(BACKWARD, deltaTime);
	if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
		camera.ProcessKeyboard(LEFT, deltaTime);
	if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
		camera.ProcessKeyboard(RIGHT, deltaTime);
}

// glfw: whenever the window size changed (by OS or user resize) this callback function executes
// ---------------------------------------------------------------------------------------------
void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
	// make sure the viewport matches the new window dimensions; note that width and 
	// height will be significantly larger than specified on retina displays.
	glViewport(0, 0, width, height);
}


// glfw: whenever the mouse moves, this callback is called
// -------------------------------------------------------
void mouse_callback(GLFWwindow* window, double xpos, double ypos)
{
	if (firstMouse)
	{
		lastX = xpos;
		lastY = ypos;
		firstMouse = false;
	}

	float xoffset = xpos - lastX;
	float yoffset = lastY - ypos; // reversed since y-coordinates go from bottom to top

	lastX = xpos;
	lastY = ypos;

	camera.ProcessMouseMovement(xoffset, yoffset);
}

// glfw: whenever the mouse scroll wheel scrolls, this callback is called
// ----------------------------------------------------------------------
void scroll_callback(GLFWwindow* window, double xoffset, double yoffset)
{
	camera.ProcessMouseScroll(yoffset);
}

//void ComputerVertexs(std::vector<Vertex>& vertexs, const std::vector<BoneFrame>& boneFrames)
//{
//	for (auto&x : vertexs)
//	{
//		glm::vec4 newPos(x.pos, 1.0);
//		glm::vec4 vPos(0.0, 0.0, 0.0, 0.0);
//		unsigned int bonesCount = x.bonesID->size();
//		for (unsigned int i = 0; i < bonesCount; ++i)
//		{
//			unsigned int index = x.bonesIndexInFrames->at(i);
//			glm::vec4 pos = boneFrames[index].m_worldMat[itimes % boneFrames[index].m_worldMat.size()] * x.vertexToBonesMat->at(i) * newPos;
//
//			pos *= x.bonesWeight->at(i);
//			vPos += pos;
//
//			//for (unsigned int j = 0; j < boneFrames.size(); ++j)
//			//{
//			//	if (x.bonesID->at(i) == boneFrames[j].boneID)
//			//	{
//			//		glm::vec4 pos = boneFrames[j].m_worldMat[0] * x.vertexToBoneMat * newPos;
//
//			//		pos *= x.bonesWeight->at(i);
//			//		vPos += pos;
//			//		break;
//			//	}
//			//}
//		}
//		x.pos = vPos;
//	}
//}

void ComputerVertexs(std::vector<Vertex>& vertexs, const std::vector<BoneFrame>& boneFrames)
{
	for (int i = 0; i < vertexs.size(); i = i + 3)
	{
		glm::vec3 vPos(vertexs[i].pos);
		glm::vec3 vPos2(vertexs[i + 1].pos);
		glm::vec3 vPos3(vertexs[i + 2].pos);


		glm::mat4 newPos(0.0);
		unsigned int bonesCount = vertexs[i].bonesID->size();
		for (unsigned int j = 0; j < bonesCount; ++j)
		{
			unsigned int index = vertexs[i].bonesIndexInFrames->at(j);
			glm::mat4 pos = boneFrames[index].m_worldMat[itimes % boneFrames[index].m_worldMat.size()] * vertexs[i].vertexToBonesMat->at(j);

			pos *= vertexs[i].bonesWeight->at(j);
			newPos += pos;
		}
		vertexs[i].pos = newPos * glm::vec4(vertexs[i].pos, 1.0);
		//vertexs[i].normal = newPos * glm::vec4(vertexs[i].normal, 1.0);

		newPos = glm::mat4(0.0);
		bonesCount = vertexs[i + 1].bonesID->size();
		for (unsigned int j = 0; j < bonesCount; ++j)
		{
			unsigned int index = vertexs[i + 1].bonesIndexInFrames->at(j);
			glm::mat4 pos = boneFrames[index].m_worldMat[itimes % boneFrames[index].m_worldMat.size()] * vertexs[i + 1].vertexToBonesMat->at(j);

			pos *= vertexs[i + 1].bonesWeight->at(j);
			newPos += pos;
		}
		vertexs[i + 1].pos = newPos * glm::vec4(vertexs[i + 1].pos, 1.0);
		//vertexs[i + 1].normal = newPos * glm::vec4(vertexs[i + 1].normal, 1.0);

		newPos = glm::mat4(0.0);
		bonesCount = vertexs[i + 2].bonesID->size();
		for (unsigned int j = 0; j < bonesCount; ++j)
		{
			unsigned int index = vertexs[i + 2].bonesIndexInFrames->at(j);
			glm::mat4 pos = boneFrames[index].m_worldMat[itimes % boneFrames[index].m_worldMat.size()] * vertexs[i + 2].vertexToBonesMat->at(j);

			pos *= vertexs[i + 2].bonesWeight->at(j);
			newPos += pos;
		}
		vertexs[i + 2].pos = newPos * glm::vec4(vertexs[i + 2].pos, 1.0);
		//vertexs[i + 2].normal = newPos * glm::vec4(vertexs[i + 2].normal, 1.0);

		//glm::vec3 newNormal = CalculateFaceNormal(vertexs[i].normal, vPos, vPos2, vPos3, vertexs[i].pos, vertexs[i + 1].pos, vertexs[i + 2].pos);
		//vertexs[i].normal = newNormal;
		//vertexs[i + 1].normal = newNormal;
		//vertexs[i + 2].normal = newNormal;
	}
}


void showBones(const Shader& shader, const std::vector<BoneFrame>& boneframes)
{
	unsigned int VAO, VBO;
	glGenVertexArrays(1, &VAO);
	glGenBuffers(1, &VBO);
	glBindVertexArray(VAO);
	glBindBuffer(GL_ARRAY_BUFFER, VBO);

	float   arBone[] =
	{
		0.0f, 0.0f, 0.0f,
		-0.2f, 0.2f,-0.2f,
		0.2f, 0.2f,-0.2f,
		0.0f, 3.0f, 0.0f,
		-0.2f, 0.2f,-0.2f,
		-0.2f, 0.2f, 0.2f,
		0.0f, 0.0f, 0.0f,
		0.2f, 0.2f,-0.2f,
		0.2f, 0.2f, 0.2f,
		0.0f, 0.0f, 0.0f,
		-0.2f, 0.2f, 0.2f,
		0.0f, 3.0f, 0.0f,
		0.2f, 0.2f, 0.2f,
		-0.2f, 0.2f, 0.2f,
	};

	glBufferData(GL_ARRAY_BUFFER, sizeof(arBone), arBone, GL_STATIC_DRAW);

	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
	glEnableVertexAttribArray(0);

	for (auto& x : boneframes)
	{
		glm::mat4 model = x.m_worldMat[itimes % x.m_worldMat.size()];
		model = glm::rotate(model, glm::radians(90.0f), glm::vec3(0.0f, 0.0f, 1.0f));
		model = glm::scale(model, glm::vec3(2, 20, 2));
		shader.setMat4("model", model);

		glDrawArrays(GL_LINE_STRIP, 0, sizeof(arBone) / 12);
	}
}

glm::vec3 CalculateFaceNormal(const glm::vec3& originNormal, const glm::vec3& point1, const glm::vec3& point2, const glm::vec3& point3,
	const glm::vec3& newpoint1, const glm::vec3& newpoint2, const glm::vec3& newpoint3)
{
	glm::vec3 normal = glm::normalize(glm::cross(point2 - point1, point3 - point1));
	bool flag = glm::dot(normal, originNormal) > 0 ? true : false;

	glm::vec3 normal2 = glm::normalize(glm::cross(newpoint2 - newpoint1, newpoint3 - newpoint1));
	if (flag)
	{
		return normal2;
	}
	else
	{
		return -normal2;
	}
}
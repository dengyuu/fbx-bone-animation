#pragma once
struct Mesh;
class FBXLoader
{
public:
	FBXLoader();
	virtual ~FBXLoader();
	bool Init();
	bool LoadModel(const char* pFilename);
	void ReadNode(FbxNode* pNode);
	const std::vector<Mesh>& getMeshs();
	const std::vector<MeshFrame>& getMeshFrames() { return m_MeshFrames; };
	const std::vector<BoneFrame>& getBoneFrames() { return m_BoneFrames; };
	const std::vector<std::vector<glm::mat4>>& getBoneMatByFrames() { return m_BoneMatFrames; };
protected:
	std::vector<Mesh> m_MeshVec;
	FbxManager* m_fbxMgr;
	FbxScene* m_scene;

	FbxAnimLayer* m_pAnimLayer;
	FbxTimeSpan m_pTimeInterval;
	std::vector<MeshFrame> m_MeshFrames;
	std::vector<BoneFrame> m_BoneFrames;
	std::vector<std::vector<glm::mat4>> m_BoneMatFrames;
	std::map<unsigned int, glm::mat4> m_BonesInverse;
};
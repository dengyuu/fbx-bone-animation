#version 450 core

const int BONES_COUNT = 100;

layout (location = 0) in vec3 aPos;
layout (location = 1) in vec3 aNormal;
layout (location = 2) in vec2 aTexCoord;
layout (location = 3) in ivec4 bonesIndexInFrames;
layout (location = 4) in vec4 bonesWeight;

out vec2 TexCoord;
out vec4 FragPos;
out vec3 Normal;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;
uniform bool hasBone;
uniform mat4 bonesMat[BONES_COUNT];

vec4 ComputeVertexByBoneMatrix();

void main()
{
	vec4 pos = vec4(aPos, 1.0);
	if(hasBone)
	{
		pos = ComputeVertexByBoneMatrix();
	}
	
	FragPos = model * pos;
	Normal = mat3(transpose(inverse(model))) * aNormal;  
	
	gl_Position = projection * view * FragPos;
	TexCoord = vec2(aTexCoord.x, aTexCoord.y);
}


vec4 ComputeVertexByBoneMatrix()
{
	mat4 matTrans =  bonesMat[bonesIndexInFrames.x] *  bonesWeight.x;
	matTrans += bonesMat[bonesIndexInFrames.y] *  bonesWeight.y;
	matTrans += bonesMat[bonesIndexInFrames.z] *  bonesWeight.z;
	matTrans += bonesMat[bonesIndexInFrames.w] *  bonesWeight.w;
	
	vec4 pos = matTrans * vec4(aPos,1.0);
	return pos;
}
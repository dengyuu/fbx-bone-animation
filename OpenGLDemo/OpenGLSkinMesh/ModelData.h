#pragma once
#include <vector>
#include <map>
#include <memory>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <fbxsdk.h>

const int NUM_BONES_PER_VEREX = 4;
struct Vertex
{
	Vertex():pos(glm::vec3(0.0)), texcoord(glm::vec2(0.0)), bonesID(glm::vec4(-1.0)), bonesIndexInFrames(glm::ivec4(0)), bonesWeight(glm::vec4(0.0)){};
	glm::vec3 pos; // 顶点在模型空间中的位置
	glm::vec3 normal; // 顶点对应法线
	glm::vec2 texcoord;// 纹理坐标
	glm::vec4 bonesID;
	glm::ivec4 bonesIndexInFrames;
	glm::vec4 bonesWeight;
	int index;
};

struct Mesh
{
	std::vector<Vertex> m_Vertex;//点
	std::string shader; // 纹理文件名
	unsigned int texID; //纹理缓冲对象
	glm::mat4 m_worldMat;//世界坐标矩阵
	unsigned int VAO, VBO/*, EBO*/;// 顶点数组对象，顶点缓冲对象，索引缓冲对象
};

struct BoneFrame
{
	unsigned int boneID;		//唯一Id
	std::vector<glm::mat4> m_worldMat;	//世界坐标矩阵
	glm::mat4 vertexToBonesMat;
};

struct MeshFrame
{
	unsigned int id;
	std::vector<glm::mat4> fpos;
};

struct StateSetContent
{
	StateSetContent()
		: diffuseFactor(1.0),
		reflectionFactor(1.0),
		emissiveFactor(1.0),
		ambientFactor(1.0)
	{
	}

	// more textures types here...
	std::string diffuseTextureName;
	std::string opacityTextureName;
	std::string reflectionTextureName;
	std::string emissiveTextureName;
	std::string ambientTextureName;
	// textures maps channels names...
	std::string diffuseChannel;
	std::string opacityChannel;
	std::string reflectionChannel;
	std::string emissiveChannel;
	std::string ambientChannel;
	// more channels names here...

	// combining factors...
	double diffuseFactor;
	double reflectionFactor;
	double emissiveFactor;
	double ambientFactor;
	// more combining factors here...

	double diffuseScaleU;
	double diffuseScaleV;
	double opacityScaleU;
	double opacityScaleV;
	double emissiveScaleU;
	double emissiveScaleV;
	double ambientScaleU;
	double ambientScaleV;

	enum TextureUnit
	{
		DIFFUSE_TEXTURE_UNIT = 0,
		OPACITY_TEXTURE_UNIT,
		REFLECTION_TEXTURE_UNIT,
		EMISSIVE_TEXTURE_UNIT,
		AMBIENT_TEXTURE_UNIT
		// more texture units here...
	};
};
typedef std::map<const FbxSurfaceMaterial *, StateSetContent> FbxMaterialMap;
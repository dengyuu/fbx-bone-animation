#version 450 core
out vec4 FragColor;

in vec3 Normal;  
in vec4 FragPos;  
in vec2 TexCoord;

// texture samplers
uniform sampler2D texture1;
uniform int type;

void main()
{
	vec3 objectColor;
	if(type == 0)
	{
		objectColor = vec3(0.5, 0.5, 0.5);
	}
	else
	{
		objectColor = vec3(texture(texture1, TexCoord));
	}
	//vec3 objectColor = vec3(texture(texture1, TexCoord));
	//vec3 objectColor = vec3(0.5, 0.5, 0.5);
	
    // ambient
    float ambientStrength = 0.2;
    vec3 ambient = ambientStrength * vec3(1.0, 1.0, 1.0); //lightColor
  	
    // diffuse 
    vec3 norm = normalize(Normal);
    vec3 lightDir = normalize(vec3(-1.0, -1.0, -1.0));
    float diff = max(dot(norm, lightDir), 0.0);
    vec3 diffuse = diff * vec3(1.0, 1.0, 1.0);
          
    vec3 result = (ambient + diffuse) * objectColor;
    FragColor = vec4(result, 1.0);
	
	//FragColor = texture(texture1, TexCoord);
}